export type Post = {
  id: number;
  content: string;
  user_id: number;
}

export type User = {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
}

export type Reaction = {
  user_id: number;
  post_id: number;
  type: string;
}