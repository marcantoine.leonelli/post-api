import express from "express";
import cors from "cors";
import postRouter from "./src/posts/postRouter";
import userRouter from "./src/users/userRouter";
import countRouter from "./src/count/countRouter";

const app = express();
let port = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//routes
app.use("/posts", postRouter);
app.use("/users", userRouter);
app.use("/count", countRouter);

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});

export default app;