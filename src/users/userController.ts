import prisma from "../../db/prisma";
import { Request, Response } from "express";
import { User, Reaction } from "../../type";

export async function getAllUsers(req: Request, res: Response) {
  const { page = 1, limit = 10 } = req.query;
  try {
    const users = await prisma.user.findMany({
      skip: (Number(page) - 1) * Number(limit),
      take: Number(limit),
    });
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function getUserById(req: Request, res: Response) {
  const { id } = req.params;

  if (isNaN(Number(id))) return res.status(400).json({ message: "Invalid id" });

  try {
    const user = await prisma.user.findUnique({
      where: {
        id: parseInt(id),
      },
      include: {
        reactions: true,
        posts: {
          where: {
            reports: {
              none: {}
            }
          }
        },
        _count: {
          select: {
            reports: true,
          },
        }
      },
    });
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function createUser(req: Request, res: Response) {
  const { username, email, first_name, last_name } = req.body;
  if (!username || !email || !first_name || !last_name)
    return res
      .status(400)
      .json({ message: "Missing username, email, first_name or last_name" });

  try {
    const user = await prisma.user.create({
      data: {
        email,
        username,
        first_name,
        last_name,
      },
    });
    console.log("user created :", user)
    res.status(201).json(user);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function updateUser(req: Request, res: Response) {
  const { id } = req.params;
  const { username, email, first_name, last_name } = req.body;
  if (!username || !email || !first_name || !last_name)
    return res
      .status(400)
      .json({ message: "Missing username, email, first_name or last_name" });

  try {
    const user = await prisma.user.update({
      where: {
        id: parseInt(id),
      },
      data: {
        username,
        email,
        first_name,
        last_name,
      },
    });
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function deleteUser(req: Request, res: Response) {
  const { id } = req.params;
  try {
    const user = await prisma.user.delete({
      where: {
        id: parseInt(id),
      },
    });
    res.status(204);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function getReactionsByUser(req: Request, res: Response) {
  const { id } = req.params;
  try {
    const reactions = await prisma.reaction.findMany({
      where: {
        user_id: parseInt(id),
      },
      include: {
        post: true,
      },
    });
    res.status(200).json(reactions);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function getAllReporters(req: Request, res: Response) {
  const { page, limit } = req.query;

  try {
    const reporters = await prisma.reports.findMany({
      skip: (Number(page) - 1) * Number(limit),
      take: Number(limit),
      include: {
        user: true,
      },
    })

    res.status(200).json(reporters);
  } catch (error) {
    res.status(500).json(error);
  }
}