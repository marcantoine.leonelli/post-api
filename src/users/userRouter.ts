import express from "express";
import {
  createUser,
  getAllUsers,
  deleteUser,
  getUserById,
  updateUser,
  getReactionsByUser,
  getAllReporters
} from "./userController";

const userRouter = express.Router();

userRouter.get("/", getAllUsers);
userRouter.post("/", createUser);
userRouter.get("/reported", getAllReporters);
userRouter.get("/:id", getUserById);
userRouter.put("/:id", updateUser);
userRouter.delete("/:id", deleteUser);
userRouter.get("/:id/reactions", getReactionsByUser);

export default userRouter;
