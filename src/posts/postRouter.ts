import express from "express";
import {
  getPosts,
  createPost,
  deletePost,
  getPostById,
  updatePost,
  addReactionToPost,
  reportPost,
  getReportedPosts
} from "./postController";

const postRouter = express.Router();

postRouter.get("/", getPosts);
postRouter.post("/", createPost);
postRouter.get("/reported", getReportedPosts);
postRouter.get("/:id", getPostById);
postRouter.put("/:userId/:postId", updatePost);
postRouter.delete("/:id", deletePost);
postRouter.post("/reactions", addReactionToPost);
postRouter.post("/report", reportPost);

export default postRouter;
