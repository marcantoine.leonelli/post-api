import prisma from "../../db/prisma";
import { Request, Response } from "express";
import { Post } from "../../type";

export async function getPosts(req: Request, res: Response) {
  const page = parseInt(req.query.page as string) || 1;
  const limit = parseInt(req.query.limit as string) || 10;

  try {
    const posts = await prisma.post.findMany({
      include: {
        user: true,
        reactions: true,
        _count: {
          select: {
            reports: true,
          },
        }
      },
      skip: page ? (page - 1) * limit : 0,
      take: limit,
    });
    res.status(200).json(posts);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function getPostById(req: Request, res: Response) {
  const { id } = req.params;

  if (isNaN(Number(id))) return res.status(400).json({ message: "Invalid id" });

  try {
    const post = await prisma.post.findUnique({
      where: {
        id: Number(id),
      },
      include: {
        user: true,
        reactions: {
          include: {
            user: true,
          },
        },
        reports: {
          include: {
            user: true,
          }
        }
      },
    });

    if (!post) return res.status(404).json({ message: "Post not found" });

    res.status(200).json(post);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function createPost(req: Request, res: Response) {
  const { content, user_id } = req.body;

  try {
    if (!content || !user_id)
      return res.status(400).json({ message: "Missing content or user_id" });

    const user = await prisma.user.findUnique({
      where: {
        id: user_id,
      },
    });

    if (!user) return res.status(400).json({ message: "User does not exist" });

    const post = await prisma.post.create({
      data: {
        content,
        user: {
          connect: {
            id: user_id,
          },
        },
      },
    });
    res.status(201).json(post);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function updatePost(req: Request, res: Response) {
  const { postId, userId } = req.params;
  const { content } = req.body;

  try {
    if (!content || !postId || !userId)
      return res
        .status(400)
        .json({ message: "Missing content, post_id or user_id" });

    const post = await prisma.post.findFirst({
      where: {
        id: parseInt(postId),
        user_id: parseInt(userId),
      },
    })

    if (!post) return res.status(404).json({ message: "Post not found" });

    const updatedPost = await prisma.post.update({
      where: {
        id: parseInt(postId),
      },
      data: {
        content,
      },
    });
    res.status(200).json(updatedPost);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function deletePost(req: Request, res: Response) {
  const { id } = req.params;
  try {
    const post = await prisma.post.delete({
      where: {
        id: parseInt(id),
      },
    });
    res.status(204);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function addReactionToPost(req: Request, res: Response) {
  const { post_id, user_id, type } = req.body;
  if (!post_id || !user_id || !type)
    return res
      .status(400)
      .json({ message: "Missing post_id, user_id or type" });

  try {
    const user = await prisma.user.findUnique({
      where: {
        id: user_id,
      },
    });

    if (!user) return res.status(400).json({ message: "User does not exist" });

    const post = await prisma.post.findUnique({
      where: {
        id: post_id,
      },
    });

    if (!post) return res.status(400).json({ message: "Post does not exist" });

    const reaction = await prisma.reaction.create({
      data: {
        type,
        user: {
          connect: {
            id: user_id,
          },
        },
        post: {
          connect: {
            id: post_id,
          },
        },
      },
    });
    res.status(201).json(reaction);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function getReportedPosts(req: Request, res: Response) {
  const page = parseInt(req.query.page as string) || 1;
  const limit = parseInt(req.query.limit as string) || 10;

  try {
    const posts = await prisma.post.findMany({
      where: {
        reports: {
          some: {}
        }
      },
      include: {
        user: true,
        reactions: true,
        _count: {
          select: {
            reports: true,
          }
        },
      },
      skip: page ? (page - 1) * limit : 0,
      take: limit,
    });
    res.status(200).json(posts);
  } catch (error) {
    res.status(500).json(error);
  }
}

export async function reportPost(req: Request, res: Response) {
  const { post_id, user_id, reason } = req.body;
  if (!post_id || !user_id)
    return res.status(400).json({ message: "Missing post_id or user_id" });

  try {
    const user = await prisma.user.findUnique({
      where: {
        id: user_id,
      },
    });

    if (!user) return res.status(400).json({ message: "User does not exist" });

    const post = await prisma.post.findUnique({
      where: {
        id: post_id,
      },
    });

    if (!post) return res.status(400).json({ message: "Post does not exist" });

    const report = await prisma.reports.create({
      data: {
        user: {
          connect: {
            id: user_id,
          },
        },
        post: {
          connect: {
            id: post_id,
          },
        },
        reason,
      },
    });
    res.status(201).json(report);
  } catch (error) {
    res.status(500).json(error);
  }
}