import express from "express";
import { getCounts } from "./countController";

const countRouter = express.Router();

countRouter.get("/", getCounts);

export default countRouter;