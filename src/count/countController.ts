import prisma from "../../db/prisma";
import { Request, Response } from "express";

export async function getCounts(req: Request, res: Response) {
  try {
    const users = await prisma.user.count();
    const posts = await prisma.post.count();
    const reactions = await prisma.reaction.count();
    const reports = await prisma.reports.count();
    const post_reported = await prisma.post.count({
      where: {
        reports: {
          some: {}
        }
      }
    })
    const user_who_reported = await prisma.user.count({
      where: {
        reports: {
          some: {}
        }
      }
    })

    res.status(200).json({
      users,
      posts,
      reactions,
      reports,
      post_reported,
      user_who_reported
    });
  } catch (error) {
    res.status(500).json(error);
  }
}

