import express from "express";
import { getReports, updateReport, deleteReport } from "./reportController";

const reportRouter = express.Router();

reportRouter.get("/", getReports);
reportRouter.put("/:user_id/:post_id", updateReport);
reportRouter.delete("/:user_id/:post_id", deleteReport);
