import prisma from "../../db/prisma";
import { Request, Response } from "express";

export const getReports = async (req: Request, res: Response) => {
  const { page = 1, limit = 10 } = req.query;

  try {
    const reports = await prisma.reports.findMany({
      take: Number(limit),
      skip: (Number(page) - 1) * Number(limit),
      include: {
        user: true,
        post: true,
      },
    });

    return res.status(200).json(reports);
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
}

export const updateReport = async (req: Request, res: Response) => {
  const { user_id, post_id } = req.params;
  const { reason } = req.body;

  try {
    const report = await prisma.reports.update({
      where: {
        user_id_post_id: {
          user_id: Number(user_id),
          post_id: Number(post_id),
        },
      },
      data: {
        reason,
      },
    });

    return res.status(200).json(report);
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
}

export const deleteReport = async (req: Request, res: Response) => {
  const { user_id, post_id } = req.params;

  try {
    const report = await prisma.reports.delete({
      where: {
        user_id_post_id: {
          user_id: Number(user_id),
          post_id: Number(post_id),
        },
      },
    });

    return res.status(204);
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
}

