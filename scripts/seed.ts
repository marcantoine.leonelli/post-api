import { faker } from '@faker-js/faker';
import prisma from '../db/prisma';
import { User, Post, Reaction } from '../type';

function generateRandomUser(index: number) {
  return {
    username: faker.internet.userName(),
    email: index + faker.internet.email(),
    first_name: faker.name.firstName(),
    last_name: faker.name.lastName(),
  };
}

function generateRandomPost() {
  return {
    content: faker.lorem.paragraph(),
  };
}

function generateRandomReaction() {
  const reactions = ['like', 'love', 'haha', 'wow', 'sad', 'angry'];
  const randomIndex = Math.floor(Math.random() * reactions.length);
  return {
    type: reactions[randomIndex],
  };
}

async function seed() {
  const start = Date.now();
  const usersCount = await prisma.user.count();
  if (usersCount > 0) {
    console.log('Database already seeded');
    return;
  }
  console.log('Seeding database...')
  // console.log('Clearing database...')
  // await prisma.reaction.deleteMany();
  // await prisma.reports.deleteMany();
  // await prisma.post.deleteMany();
  // await prisma.user.deleteMany();

  // console.log(`Database cleared in ${Date.now() - start}ms`);

  console.log("create users ...")

  const newUsers: any[] = [];

  for (let i = 0; i < 1500; i++) {
    newUsers.push(generateRandomUser(i));
  }

  await prisma.user.createMany({
    data: newUsers,
  });

  console.log("users created")

  console.log("create posts...")

  const users = await prisma.user.findMany({
    select: {
      id: true,
    }
  })

  const newPosts: any[] = [];

  for (let user of users) {
    for (let i = 0; i < 10; i++) {
      newPosts.push({
        ...generateRandomPost(),
        user_id: user.id,
      })
    }
  }

  await prisma.post.createMany({
    data: newPosts,
    skipDuplicates: true,
  });

  console.log("posts created")

  const posts = await prisma.post.findMany({
    select: {
      id: true,
    }
  })

  console.log("posts created")

  console.log("create reactions...")

  const newReactions: any[] = [];

  for (let i = 0; i < 1500; i++) {
    for (let j = 0; j < 100; j++) {
      const userId = users[i].id;

      const postIndex =  (i * 10 + j) % 15000;

      const post = posts[postIndex];

      if (!post) {
        console.log("post not found : ", postIndex, i, j)
        break;
      }

      newReactions.push({
        ...generateRandomReaction(),
        user_id: userId,
        post_id: post.id,
      })
    }
  }

  await prisma.reaction.createMany({
    data: newReactions,
    skipDuplicates: true,
  });

  console.log("create reports...")

  const newReports: any[] = [];

  for (let i = 0; i < 1000; i++) {
    const randomUser = users[Math.floor(Math.random() * users.length)];
    const randomPost = posts[Math.floor(Math.random() * posts.length)];

    newReports.push({
      user_id: randomUser.id,
      post_id: randomPost.id,
      reason: faker.lorem.sentence(),
    })
  }

  await prisma.reports.createMany({
    data: newReports,
    skipDuplicates: true,
  });

  console.log("reactions created")
  console.log('Database seeded');
  console.log(`Database seeded in ${Date.now() - start}ms`);
}

seed()

/*

for (let usersId of usersIds) {
    const reactions: any[] = [];
    for (let i = 0; i < 10; i++) {
      const randomPostId = postsIds[Math.floor(Math.random() * postsIds.length)];

      reactions.push({
        ...generateRandomReaction(),
        user_id: usersId,
        post_id: randomPostId,
      })
    }
    //console.log(`reactions: ${usersId}/${usersIds.length}`)
    await prisma.reaction.createMany({
      data: reactions,
      skipDuplicates: true,
    });
  }

  let reactionsCount = await prisma.reaction.count();
  console.log(`reactions count: ${reactionsCount}`)

  const postsWithLessThan10Reactions = await prisma.post.findMany({
    where: {
      reactions: {
        some: {
          post_id: {
            gt: 0
          }
        }
      }
    },
    select: {
      id: true,
      reactions: {
        select: {
          user_id: true,
        }
      }
    }
  })

  for (let post of postsWithLessThan10Reactions) {
    const reactions: any[] = [];

    usersIds = usersIds.filter((id) => {
      for (let reaction of post.reactions) {
        if (reaction.user_id === id) {
          return false;
        }
      }
      return true;
    });

    for (let i = 0; i < 10 - post.reactions.length; i++) {
      const randomPostId = postsIds[Math.floor(Math.random() * postsIds.length)];

      reactions.push({
        ...generateRandomReaction(),
        user_id: usersIds[Math.floor(Math.random() * usersIds.length)],
        post_id: randomPostId,
      })
    }

    await prisma.reaction.createMany({
      data: reactions,
      skipDuplicates: true,
    });
  }

  */